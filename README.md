# Running Celery functions on Docker containers
This is a project to see if you can run the tasks as docker containers in the celery workers.  The idea is that you could separate the task code from the celery code.  The benefit is that you could have a set of tasks for Facebook ad data and a separate one for Google Analytics.  Such things as packages and their sub-packages can be separate and therefore avoid version conflicts.

## Findings
- `group` can be used to check for when a set of tasks are completed
- if one task fails, then the parent task fails
    - the code after the call to `apply_async` will not run
- mounting the `/var/run/docker.sock:/var/run/docker.sock` seem to work for the python docker package
    - this is true on docker for mac.
    - haven't tested on a linux instance - something like on ec2

## Usage
- `docker-compose up -d`
- can see the runs in Flower at `localhost:8888`
- can see logs at `docker-compose logs -f`
