from celery import Celery

app = Celery(include=('tasks',))
app.conf.beat_schedule = {
    'refresh': {
        'task': 'test_delay',
        'schedule': 120,
        'args': (15,),
    },
}
