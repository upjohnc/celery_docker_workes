import datetime as dt
import time

import docker
from celery import group
from celery.utils.log import get_task_logger

from worker import app

logger = get_task_logger(__name__)


@app.task(name='test_delay')
def test_delay(worker_count):
    with open(f'file_', 'a+') as f:
        f.write('Start: \n')
        f.write(str(dt.datetime.now()))
        f.write('\n\n')
    job = group(print_thing.s(i) for i in range(worker_count))
    result = job.apply_async()
    while result.completed_count() < worker_count:
        time.sleep(1)

    with open(f'file_', 'a+') as f:
        f.write('End:\n')
        f.write(str(dt.datetime.now()))
        f.write(f'\nWhere is thing: {result.successful()}')
        f.write(f'\nCount of things:{result.completed_count()} ')


@app.task
def print_thing(i):
    client = docker.from_env()
    time.sleep(i % 100)
    message = client.containers.run("ubuntu:latest", f"echo hello world: {i}").decode()
    print(message)
    return message
